const axios = require("axios");

module.exports.getTLVWeather = async function getWeather(){
    const location ={
        latitude: 32.08,
        longitude: 34.78,
    }

    let wether = axios.get(`https://api.open-meteo.com/v1/forecast?latitude=${location.latitude}&longitude=${location.longitude}&hourly=temperature_2m`)
    .then(res =>{
        return res;
    })

    let response = await wether;
    if (response.status == 200){
        let data = combainData(response.data.hourly);
        return data;
    }
};



const combainData = (data) => {
    let newData = [];
        for (let i = 0; i < data.time.length; i++) {
            let combainData = {
                time: data.time[i],
                temperature: data.temperature_2m[i]
            }
            newData.push(combainData);
        }
    return newData;
}

