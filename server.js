const express = require("express");
const cors = require("cors");
const app = express();

const getUsers = require('./public/modules/getUsers')
const getWeather = require('./public/modules/getWeather')

app.use(cors());

app.get('/getusers', async function(req, res) {
  const params = req.query;

  if (Object.keys(params).length === 0) 
    return res.status(400).json({status: 400, message: "must have user info"});

  const data = await getUsers.getAllUsers(params);
  return res.json({ data });
});

app.get('/getweather',async function(req, res) {
    const data = await getWeather.getTLVWeather();
    return res.json({ data });
});

app.listen(8000, () => {
  console.log(`Server is running on port 8000.`);
});